<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPharmaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pharmacies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('nif', 50);
            $table->longText('heures');
            $table->longText('adresse');
            $table->longText('picture')->nullable();
            $table->string('telephone', 20);
            $table->string('whatsapp', 20)->nullable();
            $table->string('email')->unique();
            $table->decimal('latitude', 8, 2);
            $table->decimal('longitude', 8, 2);
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->integer('modify_by')->nullable();
            $table->integer('delete_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T_Pharmaies');
    }
}
