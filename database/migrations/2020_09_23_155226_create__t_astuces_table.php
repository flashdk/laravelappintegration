<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTAstucesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('astuces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->longText('description');
            $table->boolean('is_active')->default(true);
            $table->integer('created_by');
            $table->integer('modify_by')->nullable();
            $table->integer('delete_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T_Astuces');
    }
}
