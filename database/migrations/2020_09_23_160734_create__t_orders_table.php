<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->foreignId('product_id')->constrained();
            $table->integer('qte')->default(1);
            $table->boolean('status');
            $table->boolean('is_active')->default(true);
            $table->integer('created_by');
            $table->integer('modify_by')->nullable();
            $table->integer('delete_by')->nullable();
            $table->timestamps();
            $table->primary(array('id', 'product_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T_Orders');
    }
}
