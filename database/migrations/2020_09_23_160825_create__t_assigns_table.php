<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTAssignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigns', function (Blueprint $table) {
            $table->bigInteger('id');
            $table->foreignId('order_id')->references('id')->on('orders');
            $table->foreignId('pharmacy_id')->references('id')->on('pharmacies');
            $table->date('assign_date');
            $table->boolean('is_treated');
            $table->boolean('status')->default(true);
            $table->integer('created_by');
            $table->integer('modify_by')->nullable();
            $table->integer('delete_by')->nullable();
            $table->timestamps();
            $table->primary(array('id', 'order_id', 'pharmacy_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T_Assigns');
    }
}
