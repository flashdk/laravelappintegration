<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Astuces extends Model
{
    protected $fillable = [
        'name',
        'description',
        'is_active',
        'created_by',
        'modify_by',
        'delete_by'
    ];
}
