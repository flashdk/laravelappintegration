<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $with = ['produit'];

    protected $fillable = [
        'id',
        'product_id',
        'qte',
        'status',
        'is_active',
        'created_by',
        'modify_by',
        'delete_by'
    ];

    public function product()
    {
        return $this->hasMany(Products::class);
    }
}
