<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $with = ['order'];

    protected $fillable = [
        'name',
        'description',
        'price',
        'picture',
        'is_active',
        'created_by',
        'modify_by',
        'delete_by'
    ];

    public function product () {
        return $this->belongsTo(Order::class, 'product_id');
    }
}
