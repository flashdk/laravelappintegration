<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\OrderRepository;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index()
    {
        return $this->orderRepository->listOfOrders();
    }
    
    // public function getSingleItem($id)
    // {
    //     return $this->orderRepository->getSingleItem($id);
    // }

    public function create(Request $request)
    {
        $data = $request->only([
            'id',
            'product_id',
            'status',
            'qte',
            'is_active',
            'created_by',
        ]);
        $validator = Validator::make($data, [
            'product_id' => 'required'
        ],[
            'product_id.required' => 'the product id of order is required'
        ]);
        if ($validator->fails()){
            return response()->json(['status' => false, 'errors' => $validator->errors()], 422);
        }
        if ($this->orderRepository->create($data)){
            return response()->json($data, 201);
        }
        return ['status' => false];
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['id'] = $id;
        $validator = Validator::make($data,[
            'id' => 'required|exists:orders,id'
        ]);
        if ($validator->fails()){
            return response()->json(['status' => false, 'errors' => $validator->errors()], 422);
        }
        if ($this->orderRepository->update($data, $id)){
            return response()->json($data, 200);
        }
        return ['status' => false];
    }

    public function getOrderAndProducts()
    {
        return $this->orderRepository->getOrderAndProducts();
    }

}
