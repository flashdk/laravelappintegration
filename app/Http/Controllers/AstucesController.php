<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\AstuceRepository;
use Illuminate\Support\Facades\Validator;

class AstucesController extends Controller
{
    private $astuceRepository;

    public function __construct(AstuceRepository $astuceRepository)
    {
        $this->astuceRepository = $astuceRepository;
    }

    public function index()
    {
        return $this->astuceRepository->listOfAstuces();
    }
    
    public function getSingleItem($id)
    {
        dd('toto');
        return $this->astuceRepository->getSingleItem($id);
    }

    public function create(Request $request)
    {
        $data = $request->only([
            'name',
            'description',
            'is_active',
            'created_by',
            'modify_by',
            'delete_by'
        ]);
        $validator = Validator::make($data, [
            'name' => 'required'
        ],[
            'name.required' => 'the name of astuce is required'
        ]);
        if ($validator->fails()){
            return response()->json(['status' => false, 'errors' => $validator->errors()], 422);
        }
        if ($this->astuceRepository->create($data)){
            return response()->json($data, 201);
        }
        return ['status' => false];
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['id'] = $id;
        $validator = Validator::make($data,[
            'id' => 'required|exists:astuces,id'
        ]);
        if ($validator->fails()){
            return response()->json(['status' => false, 'errors' => $validator->errors()], 422);
        }
        if ($this->astuceRepository->update($data, $id)){
            return response()->json($data, 200);
        }
        return ['status' => false];
    }

}
