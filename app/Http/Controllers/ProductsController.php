<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\ProductRepository;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        return $this->productRepository->listOfProducts();
    }

    public function create(Request $request)
    {
        $data = $request->only([
            'name',
            'description',
            'price',
            'picture',
            'is_active',
            'created_by',
            'modify_by',
            'delete_by'
        ]);
        $validator = Validator::make($data, [
            'name' => 'required'
        ],[
            'name.required' => 'the name of product is required'
        ]);
        if ($validator->fails()){
            return response()->json(['status' => false, 'errors' => $validator->errors()], 422);
        }
        if ($this->productRepository->create($data)){
            return response()->json($data, 201);
        }
        return ['status' => false];
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['id'] = $id;
        $validator = Validator::make($data,[
            'id' => 'required|exists:products,id'
        ]);
        if ($validator->fails()){
            return response()->json(['status' => false, 'errors' => $validator->errors()], 422);
        }
        if ($this->productRepository->update($data, $id)){
            return response()->json($data, 200);
        }
        return ['status' => false];
    }

}
