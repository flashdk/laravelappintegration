<?php


namespace App\Repository;


use App\Models\Products;
use Illuminate\Support\Facades\DB;

class ProductRepository
{
    /**
     * @var Product
     */
    private $Product;

    public function __construct(Products $Product)
    {
        $this->Product = $Product;
    }

    public function listOfProducts()
    {
        return $this->Product->newQuery()
            ->select('*')->get();
    }

    public function create(array $data)
    {
        if ($this->Product->newQuery()->create($data)){
            return true;
        }
        return false;
    }

    public function update(array $data, $id)
    {
        return DB::transaction(function () use ($data, $id){
            unset($data['id']);
            if ( $this->Product->newQuery()->where('id','=',$id)->update($data)){
                //$id = $this->Product->newQuery()->create($data)->id;
                return true;
            }
            return false;
        });
    }
}
