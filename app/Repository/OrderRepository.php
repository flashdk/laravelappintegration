<?php


namespace App\Repository;


use App\Models\Orders;
use App\Models\Products;
use Illuminate\Support\Facades\DB;

class OrderRepository
{
    /**
     * @var Order
     */ 
    private $order;
    private $product;

    public function __construct(Orders $order, Products $product)
    {
        $this->order = $order;
    }

    public function listOfOrders()
    {
        return $this->order->newQuery()
            ->select('*')->get();
    }

    public function create(array $data)
    {
        if ($this->order->newQuery()->create($data)){
            return true;
        }
        return false;
    }

    public function update(array $data, $id)
    {
        return DB::transaction(function () use ($data, $id){
            unset($data['id']);
            if ( $this->order->newQuery()->where('id','=',$id)->update($data)){
                //$id = $this->order->newQuery()->create($data)->id;
                return true;
            }
            return false;
        });
    }

    // public function getSingleItem($id)
    // {
    //     //dd('toto');
    //     return $this->order->newQuery()->where('id','=',$id)
    //     ->select('*')->get();
    // }

    public function getOrderAndProducts()
    {
        return $this->product->newQuery()
            ->get();
    }
}
