<?php


namespace App\Repository;


use App\Models\Astuces;
use Illuminate\Support\Facades\DB;

class AstuceRepository
{
    /**
     * @var Astuce
     */
    private $astuce;

    public function __construct(Astuces $astuce)
    {
        $this->astuce = $astuce;
    }

    public function listOfAstuces()
    {
        return $this->astuce->newQuery()
            ->select('*')->get();
    }

    public function create(array $data)
    {
        if ($this->astuce->newQuery()->create($data)){
            return true;
        }
        return false;
    }

    public function update(array $data, $id)
    {
        return DB::transaction(function () use ($data, $id){
            unset($data['id']);
            if ( $this->astuce->newQuery()->where('id','=',$id)->update($data)){
                //$id = $this->astuce->newQuery()->create($data)->id;
                return true;
            }
            return false;
        });
    }

    public function getSingleItem($id)
    {
        return $this->astuce->newQuery()->where('id','=',$id)
        ->select('*')->get();
    }
}
