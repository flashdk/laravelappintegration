<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| dd(bcrypt('password'))
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function (){
    Route::prefix('astuces')->group(function (){
        Route::get('/', [\App\Http\Controllers\AstucesController::class, 'index']);
        Route::post('/create', [\App\Http\Controllers\AstucesController::class, 'create']);
        Route::put('/update/{id}', [\App\Http\Controllers\AstucesController::class, 'update']);
    }); 
    
    Route::prefix('products')->group(function (){
        Route::get('/', [\App\Http\Controllers\ProductsController::class, 'index']);
        Route::get('/{id}', [\App\Http\Controllers\ProductsController::class, 'getSingleItem']);
        Route::post('/create', [\App\Http\Controllers\ProductsController::class, 'create']);
        Route::post('/update/{id}', [\App\Http\Controllers\ProductsController::class, 'update']);
    });

    Route::prefix('orders')->group(function (){
        Route::get('/', [\App\Http\Controllers\OrdersController::class, 'index']);
        Route::get('/{id}', [\App\Http\Controllers\OrdersController::class, 'getSingleItem']);
        Route::post('/create', [\App\Http\Controllers\OrdersController::class, 'create']);
        Route::post('/update/{id}', [\App\Http\Controllers\OrdersController::class, 'update']);
        
        Route::get('/orderAndProduct', [\App\Http\Controllers\OrdersController::class, 'getOrderAndProducts']);
    });
});

